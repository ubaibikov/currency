package store

import (
	"encoding/json"
	"net/http"
	"time"
	config "work/currency/configs"
	"work/currency/store/model"

	"github.com/jmoiron/sqlx"
)

// CurrencyRepository ...
type CurrencyRepository struct {
	DB     *sqlx.DB
	Config *config.API
}

// UpdateFromAPI ...
func (r *CurrencyRepository) UpdateFromAPI() error {
	curs := []model.Currency{}

	if err := r.DB.Select(&curs, "select * from currency"); err != nil {
		return err
	}

	for _, data := range curs {
		pairs := data.Cur1 + data.Cur2

		client := http.Client{}
		request, _ := http.NewRequest("GET", r.Config.Link+pairs+"&key="+r.Config.Key, nil)

		resp, _ := client.Do(request)

		var result map[string]map[string]interface{}
		json.NewDecoder(resp.Body).Decode(&result)
		currentCource := result["data"][pairs]

		if currentCource != nil {
			sql := `UPDATE currency SET cource = $1, updated_at = $2 WHERE cur1 = $3 AND cur2 = $4`

			if _, err := r.DB.Exec(sql, currentCource, time.Now(), data.Cur1, data.Cur2); err != nil {
				return err
			}
		}
	}
	return nil
}

// Find ...
func (r *CurrencyRepository) Find(cur1, cur2 string) (*model.Currency, error) {

	cur := model.Currency{}
	err := r.DB.Get(&cur, "SELECT * FROM currency WHERE cur1 = $1 AND cur2 = $2 LIMIT 1", cur1, cur2)
	if err != nil {
		return nil, err
	}

	return &cur, nil
}

// Create ...
func (r *CurrencyRepository) Create(cur1, cur2 string) {
	r.DB.MustExec("INSERT INTO currency (cur1, cur2) VALUES ($1,$2)", cur1, cur2)
}
