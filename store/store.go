package store

import (
	"log"
	config "work/currency/configs"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" //
)

//Store ...
type Store struct {
	Config *config.Config
	db     *sqlx.DB
}

// Open ...
func (s *Store) Open() {
	db, err := sqlx.Connect("postgres", s.Config.DBLink)
	if err != nil {
		log.Fatal(err)
	}

	s.db = db
}

// Close ....
func (s *Store) Close() {
	s.db.Close()
}

// Currency ...
func (s *Store) Currency() *CurrencyRepository {
	return &CurrencyRepository{
		DB:     s.db,
		Config: s.Config.API,
	}
}
