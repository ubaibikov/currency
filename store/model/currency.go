package model

import "database/sql"

// Currency ...
type Currency struct {
	ID        int64          `json:"id" db:"id"`
	Cur1      string         `json:"first_currency" db:"cur1"`
	Cur2      string         `json:"second_currency" db:"cur2"`
	Cource    sql.NullString `json:"cource" db:"cource"`
	UpdatedAt string         `json:"last_update" db:"updated_at"`
}
