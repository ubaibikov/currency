package server

import (
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"
	config "work/currency/configs"
	"work/currency/store"

	"github.com/gin-gonic/gin"
)

// Server ...
type Server struct {
	Config  *config.Config
	Store   *store.Store
	errChan chan struct{}
}

var wg sync.WaitGroup

// Start ...
func (s *Server) Start() {
	r := gin.Default()
	defer s.Store.Close()
	defer wg.Wait()
	s.Store.Open()

	s.configureRoutes(r)

	wg.Add(1)
	go s.configureWorks()
	r.Run(s.Config.Port)
}

func (s *Server) configureWorks() {
	defer wg.Wait()
	s.errChan = make(chan struct{}, 1)
	wg.Add(2)
	go s.updateCurrencies()
	go s.errorHandler()
}

func (s *Server) configureRoutes(r *gin.Engine) {
	r.GET("/api/convert", s.convertAction)
	r.POST("/api/create", s.createAction)
}

func (s *Server) convertAction(c *gin.Context) {
	curFrom := c.Query("currencyFrom")
	curTo := c.Query("currencyTo")

	cur, err := s.Store.Currency().Find(curFrom, curTo)
	if err != nil {
		log.Fatal(err)
		c.JSON(http.StatusOK, gin.H{
			"message": "so sorry we have an errors",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"value": cur.Cource.String,
	})
}

func (s *Server) createAction(c *gin.Context) {
	cur1 := c.PostForm("currency1")
	cur2 := c.PostForm("currency2")

	s.Store.Currency().Create(cur1, cur2)

	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
	})
}

func (s *Server) updateCurrencies() {
	defer wg.Done()
	for {
		time.Sleep(time.Duration(s.Config.Tick) * time.Second)
		if err := s.Store.Currency().UpdateFromAPI(); err != nil {
			s.errChan <- struct{}{}
			return
		}
		fmt.Println("Обновили данные")
	}
}

func (s *Server) errorHandler() {
	defer wg.Done()
	for {
		select {
		case <-s.errChan:
			fmt.Println("произошла какая-то ошибка остановили ловитель ошибок.")
			return
		}
	}
}
