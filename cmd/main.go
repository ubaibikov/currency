package main

import (
	config "work/currency/configs"
	"work/currency/internal/server"
	"work/currency/store"
)

func main() {
	config := config.New()

	s := store.Store{
		Config: config,
	}

	r := server.Server{
		Config: config,
		Store:  &s,
	}

	r.Start()
}
