package config

// Config ...
type Config struct {
	Port   string
	Tick   int32
	DBLink string
	API    *API
}

// API ...
type API struct {
	Link string
	Key  string
}

// New ...
func New() *Config {
	return &Config{
		Port:   ":8080",
		Tick:   5,
		DBLink: "host=localhost user=postgres password=123 port=5432 dbname=cur sslmode=disable",
		API: &API{
			"https://currate.ru/api/?get=rates&pairs=",
			"0dd2f8de2103c5528c8c94a59a5c268e",
		},
	}
}
